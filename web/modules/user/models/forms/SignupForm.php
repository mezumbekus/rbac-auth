<?php
namespace app\modules\user\models\forms;

use Yii;
use yii\base\Model;
use app\modules\user\models\User;

class SignupForm extends Model
{
    public $username;

    public function rules()
    {
        return [
            ['username','match','pattern' => '/^([\d|\w]\.?)+@[\d|\w]+\.[\d|\w]+|\+?(\d\s?\-?)+$/','message' => 'Поле должно содержать email или номер телефона'],
            ['username','required','message' => 'Поле не должно быть пустым'],
            ['username','checkExistsUsername'],
        ];
    }

    public function checkExistsUsername($attribute,$params)
    {
        return User::find()->where(['username' => $this->username])->exists();
    }
}