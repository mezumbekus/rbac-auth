<?php
namespace app\modules\user\models\forms;

use Yii;
use yii\base\Model;
use app\modules\user\models\User;

class CreatePassword extends Model
{
    public $password;
    public $password_repeat;

    public function rules()
    {
        return [
            ['password','compare','compareAttribute' => 'password_repeat'],
        ];
    }
}