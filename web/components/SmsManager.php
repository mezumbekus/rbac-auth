<?php
namespace app\components;
use yii\base\Component;
use Yii;
use yii\helpers\Url;

class SmsManager extends Component
{
    private $api_id;
    private $url;
    private $to;
    private $msg;

    public function __construct()
    {
        $this->api_id = Yii::$app->params['smsAPI'];
        $this->url = Yii::$app->params['smsURI'];
    }

    public function send(array $params)
    {
        $this->to = $params['to'];
        $this->msg = $params['msg'];
        $ch = curl_init($this->url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
            "api_id" => $this->api_id,
            "to" => $this->to,
            "msg" => iconv("windows-1251", "utf-8", $this->msg),
            "json" => 1
        )));
        $body = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($body);
        return $response;

    }

    private function makeUrl()
    {
        $url = Url::toRoute([$this->url,'api_id' => $this->api_id,'to' => $this->to,'msg' => $this->msg,'json'=>1]);
        return $url;
    }


}