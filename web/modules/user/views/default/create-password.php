<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$form = ActiveForm::begin();
echo $form->field($model,'password');
echo $form->field($model,'password_repeat');
echo Html::submitButton('Подвердить');
ActiveForm::end();