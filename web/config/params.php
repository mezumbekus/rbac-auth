<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'emailRegExp' => '/^([\d|\w]\.?)+@[\d|\w]+\.[\d|\w]+$/',
    'phoneRegExp' => '/^\+?(\d\s?\-?)+$/',
    'smsAPI' => '090DAA5B-37E4-569C-6515-C35B27F0998F',
    'smsURI' => 'https://sms.ru/sms/send'
];
