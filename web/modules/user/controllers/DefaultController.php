<?php

namespace app\modules\user\controllers;

use Yii;
use yii\base\Exception;
use yii\web\Controller;
use app\modules\user\models\forms\LoginForm;
use app\modules\user\models\forms\SignupForm;
use yii\web\Response;
use app\modules\user\models\User;
use app\modules\user\models\forms\CreatePassword;

/**
 * Default controller for the `user` module
 */
class DefaultController extends Controller
{

    public function actionIndex()
    {
        return $this->render('index');

    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionSignup()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new SignupForm();
        if($model->load(Yii::$app->request->post()) && $model->validate()) {
            $user = new User();
            $username = $model->username;
            $token = Yii::$app->security->generateRandomString(64);
            if(User::isPhone($username)) {
                $password = Yii::$app->security->generateRandomString(10);
                $hash_password = Yii::$app->security->generatePasswordHash($password);
                $sms = Yii::$app->sms;
                $response = $sms->send([
                    'to' => $username,
                    'msg' => "Ваш пароль: {$password}",
                ]);
                if($response->status_code == 100) {
                    $user->username = $username;
                    $user->phonenumber = $username;
                    $user->password_hash = $hash_password;
                    $user->auth_key = $token;
                    $user->access_token = $token;

                    if($user->save()){
                        $auth = Yii::$app->authManager;
                        $user_role = $auth->getRole('user');
                        $auth->assign($user_role,$user);
                        $this->redirect('/user/default/login');
                    }
                }
                    throw new Exception('Что-то пошло ни так!');

            }else {
                $mailer = Yii::$app->mailer;
                $user = new User();
                $user->username = $username;
                $user->email = $username;
                $user->access_token = $token;
                $user->status = User::STATUS_INACTIVE;
                if($user->save()) {
                    $auth = Yii::$app->authManager;
                    $user_role = $auth->getRole('user');
                    $auth->assign($user_role,$user);
                    $mailer->compose()
                        ->setFrom('from@domain.com')
                        ->setTo($username)
                        ->setSubject('Доступы')
                        ->setHtmlBody("Нажмите на ссылку для создания пароля <a href='127.0.0.1/create-password/'{$token}>Кликните сюда</a>")
                        ->send();
                   return $this->goHome();
                }

            }


        }
        return $this->render('signup',[
            'model' => $model,
        ]);
    }
    public function actionCreatePassword($token)
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new CreatePassword();
        if($model->load(Yii::$app->request->post())){
            $user = User::findIdentityByAccessToken($token);
            $user->status = User::STATUS_ACTIVE;
            $user->password_hash = Yii::$app->security->generatePasswordHash($model->password);
            if($user->save() && Yii::$app->user->login($user))
                return $this->goHome();
        }
        return $this->render('create-password',[
            'model' => $model,
        ]);

    }
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
