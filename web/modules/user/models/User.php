<?php

namespace app\modules\user\models;
use yii\db\ActiveRecord;
use Yii;

class User extends ActiveRecord implements \yii\web\IdentityInterface
{

    public $roles;

    const ROLE_ADMIN = 'admin';
    const ROLE_MANAGER = 'manager';
    const ROLE_USER = 'user';

    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'inactive';

    public static function isPhone($username)
    {
        if(preg_match(Yii::$app->params['phoneRegExp'],$username)){
            return true;
        }
        return false;
    }

    public static function findIdentity($id)
    {
        return self::findOne(['id' => $id]);
    }



    public static function findIdentityByAccessToken($token, $type = null)
    {
       return self::findOne(['access_token' => $token]);
    }

    public static function findByUsername($username)
    {
        return self::findOne(['email' => $username,'status' => User::STATUS_ACTIVE]);
    }
    public static function findByEmail($email)
    {
        return self::findOne(['email' => $email,'status' => User::STATUS_ACTIVE]);
    }

    public static function findByPhone($phone)
    {
        return self::findOne(['phone' => $phone,'status' => User::STATUS_ACTIVE]);
    }


    public function getId()
    {
        return $this->id;
    }


    public function getAuthKey()
    {
        return $this->authKey;
    }


    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }


    public function validatePassword($password)
    {
        return $this->password_hash === Yii::$app->security->generatePasswordHash($password);
    }
}
