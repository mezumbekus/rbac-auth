<?php
namespace app\commands;

use yii\console\Controller;
use Yii;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll();
        $adminRole = $auth->createRole('admin');
        $managerRole = $auth->createRole('manager');
        $userRole = $auth->createRole('user');
        $viewUsers = $auth->createPermission('viewUsers');
        $editUsers = $auth->createPermission('editUsers');
        $deleteUsers = $auth->createPermission('deleteUsers');
        $auth->add($adminRole);
        $auth->add($managerRole);
        $auth->add($userRole);
        $auth->add($viewUsers);
        $auth->add($editUsers);
        $auth->add($deleteUsers);
        $auth->addChild($userRole,$viewUsers);
        $auth->addChild($managerRole,$userRole);
        $auth->addChild($managerRole,$editUsers);
        $auth->addChild($adminRole,$managerRole);
        $auth->addChild($adminRole,$deleteUsers);
    }

}